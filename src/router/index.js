import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/login',
    component: () => import('../layouts/LoginLayout.vue'),
    children: [
      {
        path: '',
        name: 'Login',
        component: () => import('../views/Auth/Login.vue')
      },
      {
        path: '/confirm-email',
        name: 'ConfirmEmail',
        component: () => import('../views/Auth/ConfirmEmail.vue')
      },
      {
        path: '/reset-password',
        name: 'ResetPassword',
        component: () => import('../views/Auth/ResetPassword.vue')
      }
    ],
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('auth')) {
        next({ name: 'Welcome' });
      } else {
        next();
      }
    }

  },
  {
    path: '/welcome',
    component: () => import('../layouts/MainAppLayout'),
    children: [
      {
        path: '/',
        name: 'Welcome',
        component: () => import('../views/Welcome.vue'),
      },
    ],
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/plans',
    component: () => import('../layouts/MainAppLayout'),
    children: [
      {
        path: '/',
        name: 'Plans',
        component: () => import('../views/Plans/Plans.vue'),
      },      
    ],
    meta: {
      requiredAuth: true
    }

  },
  {
    path: '/:patchMatch(.*)',
    name: 'NotFound',
    component: () => import('../views/NotFound.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  const protectedRoute = to.matched.some(record => record.meta.requiredAuth);

  if (protectedRoute && !localStorage.getItem('auth')) {
    next({ name: 'Login' });
  } else {
    next();
  }

});


export default router
