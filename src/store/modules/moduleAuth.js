const moduleAuth = {
    namespaced: true,

    state: {
        resetPassword: null
    },

    getters:{
        
    },
    
    mutations: {
        setResetPassword(state, {email, token}){
            state.resetPassword = {
                email,
                token
            }
        },
        clearResetPassword(state){
            state.resetPassword = null;
        }
        
    },
    actions: {
    },
}

export default moduleAuth;