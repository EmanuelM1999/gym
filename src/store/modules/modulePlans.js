import axios from 'axios';

const modulePlans = {
    namespaced: true,

    state: {
        plans: null,
        page: 1,
        length_pagination: 0,
        perPage: 11,
        plan:{},
        snackbar:{
          show:false,
          timeout:2000,
          text:'',
          color:''
        },
    },

    getters: {
        getPlans(state) { return state.plans },
        getLengthPagination(state) { return state.length_pagination },
        getPerPage(state) { return state.perPage },
        getPlan(state){ return state.plan},
        getSnackbar(state){ return state.snackbar}
    },

    actions: {
         getAllPlans({ commit, state }, name) {
            axios.get('/plans?&filter[name]=' + name + '&perPage=' + state.perPage + '&page=' + state.page)
                .then(response => {

                    //Obtiene los planes en la peticion
                    let resp = response.data;
                    //Obtiene el numero de paginas
                    let last_page = response.data.last_page;

                    commit('setPlans', resp.data);
                    commit('setLengthPagination', last_page);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        async changeStatus({ commit, dispatch }, id) {
            await axios.delete('/plans/' + id)
                .then(() => {
                    let successMsg = {
                        show : true,
                        text : 'Actualizado con exito',
                        color : 'success',
                    }
                    commit('setSnackbar', successMsg)
                    dispatch('getAllPlans', "");
                })
                .catch(function (error) {
                     console.log(error);
                });
        }
    },

    mutations: {
        setPlans(state, plans) {
            state.plans = plans
        },
        setPage(state, page) {
            state.page = page;
        },
        setLengthPagination(state, length) {
            state.length_pagination = length;
        },
        setPlan(state,plan){
            state.plan = plan
        },
        setSnackbar(state, newValue){
            state.snackbar.show = newValue.show
            state.snackbar.text = newValue.text
            state.snackbar.color= newValue.color
        }
    },
}

export default modulePlans;