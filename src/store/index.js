import Vue from 'vue'
import Vuex from 'vuex'
import moduleAuth from './modules/moduleAuth.js'
import modulePlans from './modules/modulePlans.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userAuth: null,
    token: null
  },
  getters: {
    getUserAuth(state){
      return state.userAuth
    }
  },
  mutations: {
    login(state, auth) {
      state.userAuth = auth.user;
      state.token = auth.token;
    },
    logout(state) {
      state.userAuth = null;
      state.token = null;
    }
  },
  actions: {
    setAuth({ commit }) {
      if (localStorage.getItem('auth')) {
        commit('login', JSON.parse(localStorage.getItem('auth')));
      }
    }
  },
  modules: {
    auth: moduleAuth,
    plans: modulePlans
  }
})
